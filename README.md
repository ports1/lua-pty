lua-pty v1.25
=
pty (pseudo-terminal) bindings for Lua 5+

Currently hosted on [GitLab](https://gitlab.com/ports1/lua-pty)

Feedback, pull requests and Comments welcomed
